package docker03.spring.boot.docker03springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Docker03SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Docker03SpringBootApplication.class, args);
	}

}
