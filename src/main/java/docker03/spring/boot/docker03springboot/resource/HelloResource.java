package docker03.spring.boot.docker03springboot.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/docker/helo")
public class HelloResource {

    @GetMapping
    public String hello() { return "Docker no AR e Spring Boot!"; }
}
