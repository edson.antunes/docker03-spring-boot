FROM openjdk:12
ADD target/docker03-spring-boot.jar docker03-spring-boot.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker03-spring-boot.jar"]
